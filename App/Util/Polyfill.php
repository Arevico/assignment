<?php

if (!function_exists('array_key_last')){
	function array_key_last(array $arr)
	{
		end($arr);
		return key($arr);
	}
}
