
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:BundleEngine" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine.html">BundleEngine</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:BundleEngine_Model" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Model.html">Model</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:BundleEngine_Model_Analytics" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Model/Analytics.html">Analytics</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngine_Model_Analytics_CostContract" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Analytics/CostContract.html">CostContract</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:BundleEngine_Model_Bundle" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Model/Bundle.html">Bundle</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngine_Model_Bundle_Bundle" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/Bundle.html">Bundle</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Bundle_BundleA" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/BundleA.html">BundleA</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Bundle_BundleB" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/BundleB.html">BundleB</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Bundle_BundleC" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/BundleC.html">BundleC</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Bundle_BundleD" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/BundleD.html">BundleD</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Bundle_Usage" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Bundle/Usage.html">Usage</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:BundleEngine_Model_Product" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Model/Product.html">Product</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngine_Model_Product_BillableContract" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Product/BillableContract.html">BillableContract</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Product_Invoice" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Product/Invoice.html">Invoice</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Model_Product_InvoiceLine" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="BundleEngine/Model/Product/InvoiceLine.html">InvoiceLine</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:BundleEngine_Model_Customer" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="BundleEngine/Model/Customer.html">Customer</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:BundleEngine_Repository" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Repository.html">Repository</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngine_Repository_Customer" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="BundleEngine/Repository/Customer.html">Customer</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:BundleEngine_Service" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngine/Service.html">Service</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngine_Service_BestBundle" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="BundleEngine/Service/BestBundle.html">BestBundle</a>                    </div>                </li>                            <li data-name="class:BundleEngine_Service_BundleProfitRanker" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="BundleEngine/Service/BundleProfitRanker.html">BundleProfitRanker</a>                    </div>                </li>                </ul></div>                </li>                </ul></div>                </li>                            <li data-name="namespace:BundleEngineTests" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="BundleEngineTests.html">BundleEngineTests</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:BundleEngineTests_BestBundleTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/BestBundleTest.html">BestBundleTest</a>                    </div>                </li>                            <li data-name="class:BundleEngineTests_BundleTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/BundleTest.html">BundleTest</a>                    </div>                </li>                            <li data-name="class:BundleEngineTests_CustomerTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/CustomerTest.html">CustomerTest</a>                    </div>                </li>                            <li data-name="class:BundleEngineTests_ExampleTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/ExampleTest.html">ExampleTest</a>                    </div>                </li>                            <li data-name="class:BundleEngineTests_InvoiceTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/InvoiceTest.html">InvoiceTest</a>                    </div>                </li>                            <li data-name="class:BundleEngineTests_UsageTest" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="BundleEngineTests/UsageTest.html">UsageTest</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": "BundleEngine.html", "name": "BundleEngine", "doc": "Namespace BundleEngine"},{"type": "Namespace", "link": "BundleEngineTests.html", "name": "BundleEngineTests", "doc": "Namespace BundleEngineTests"},{"type": "Namespace", "link": "BundleEngine/Model.html", "name": "BundleEngine\\Model", "doc": "Namespace BundleEngine\\Model"},{"type": "Namespace", "link": "BundleEngine/Model/Analytics.html", "name": "BundleEngine\\Model\\Analytics", "doc": "Namespace BundleEngine\\Model\\Analytics"},{"type": "Namespace", "link": "BundleEngine/Model/Bundle.html", "name": "BundleEngine\\Model\\Bundle", "doc": "Namespace BundleEngine\\Model\\Bundle"},{"type": "Namespace", "link": "BundleEngine/Model/Product.html", "name": "BundleEngine\\Model\\Product", "doc": "Namespace BundleEngine\\Model\\Product"},{"type": "Namespace", "link": "BundleEngine/Repository.html", "name": "BundleEngine\\Repository", "doc": "Namespace BundleEngine\\Repository"},{"type": "Namespace", "link": "BundleEngine/Service.html", "name": "BundleEngine\\Service", "doc": "Namespace BundleEngine\\Service"},
            {"type": "Interface", "fromName": "BundleEngine\\Model\\Analytics", "fromLink": "BundleEngine/Model/Analytics.html", "link": "BundleEngine/Model/Analytics/CostContract.html", "name": "BundleEngine\\Model\\Analytics\\CostContract", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Analytics\\CostContract", "fromLink": "BundleEngine/Model/Analytics/CostContract.html", "link": "BundleEngine/Model/Analytics/CostContract.html#method_calculateCostOfGoodsSold", "name": "BundleEngine\\Model\\Analytics\\CostContract::calculateCostOfGoodsSold", "doc": "&quot;Calculate the cost of producing, creating or delivering a product.&quot;"},
            
            {"type": "Interface", "fromName": "BundleEngine\\Model\\Product", "fromLink": "BundleEngine/Model/Product.html", "link": "BundleEngine/Model/Product/BillableContract.html", "name": "BundleEngine\\Model\\Product\\BillableContract", "doc": "&quot;Make a product billable.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\BillableContract", "fromLink": "BundleEngine/Model/Product/BillableContract.html", "link": "BundleEngine/Model/Product/BillableContract.html#method_generateInvoice", "name": "BundleEngine\\Model\\Product\\BillableContract::generateInvoice", "doc": "&quot;Generate an invoice for this specific bundle&quot;"},
            
            {"type": "Interface", "fromName": "BundleEngine\\Repository", "fromLink": "BundleEngine/Repository.html", "link": "BundleEngine/Repository/Customer.html", "name": "BundleEngine\\Repository\\Customer", "doc": "&quot;The customer repository is resposible for bridging the input and output of the\ndata access layer and the local model. It has not been implemented in this example&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_get", "name": "BundleEngine\\Repository\\Customer::get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_find", "name": "BundleEngine\\Repository\\Customer::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_list", "name": "BundleEngine\\Repository\\Customer::list", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_listWhere", "name": "BundleEngine\\Repository\\Customer::listWhere", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_save", "name": "BundleEngine\\Repository\\Customer::save", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_delete", "name": "BundleEngine\\Repository\\Customer::delete", "doc": "&quot;&quot;"},
            
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/BestBundleTest.html", "name": "BundleEngineTests\\BestBundleTest", "doc": "&quot;Integration tests from the customer perspective.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\BestBundleTest", "fromLink": "BundleEngineTests/BestBundleTest.html", "link": "BundleEngineTests/BestBundleTest.html#method_testBetterBundle", "name": "BundleEngineTests\\BestBundleTest::testBetterBundle", "doc": "&quot;Test if a better bundle can be found and if the best bundle can be selected.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/BundleTest.html", "name": "BundleEngineTests\\BundleTest", "doc": "&quot;Integration tests from the customer perspective.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\BundleTest", "fromLink": "BundleEngineTests/BundleTest.html", "link": "BundleEngineTests/BundleTest.html#method_testBundle", "name": "BundleEngineTests\\BundleTest::testBundle", "doc": "&quot;Undocumented function&quot;"},
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/CustomerTest.html", "name": "BundleEngineTests\\CustomerTest", "doc": "&quot;Integration tests from the customer perspective.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\CustomerTest", "fromLink": "BundleEngineTests/CustomerTest.html", "link": "BundleEngineTests/CustomerTest.html#method_testDataUsageAbroad", "name": "BundleEngineTests\\CustomerTest::testDataUsageAbroad", "doc": "&quot;Test data usage when a customer travels abroad.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/ExampleTest.html", "name": "BundleEngineTests\\ExampleTest", "doc": "&quot;This test cases are to verify the example to check if the implementation\nis valid for the example given in the assessment pdf.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\ExampleTest", "fromLink": "BundleEngineTests/ExampleTest.html", "link": "BundleEngineTests/ExampleTest.html#method_testExampleBundleC", "name": "BundleEngineTests\\ExampleTest::testExampleBundleC", "doc": "&quot;Test bundleD case named in the pdf.&quot;"},
                    {"type": "Method", "fromName": "BundleEngineTests\\ExampleTest", "fromLink": "BundleEngineTests/ExampleTest.html", "link": "BundleEngineTests/ExampleTest.html#method_testExampleBundleD", "name": "BundleEngineTests\\ExampleTest::testExampleBundleD", "doc": "&quot;Test bundleD case named in the pdf.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/InvoiceTest.html", "name": "BundleEngineTests\\InvoiceTest", "doc": "&quot;Test the invoicing system.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\InvoiceTest", "fromLink": "BundleEngineTests/InvoiceTest.html", "link": "BundleEngineTests/InvoiceTest.html#method_testInvoice", "name": "BundleEngineTests\\InvoiceTest::testInvoice", "doc": "&quot;Test if invoicing works properly.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngineTests", "fromLink": "BundleEngineTests.html", "link": "BundleEngineTests/UsageTest.html", "name": "BundleEngineTests\\UsageTest", "doc": "&quot;Test the usage object.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngineTests\\UsageTest", "fromLink": "BundleEngineTests/UsageTest.html", "link": "BundleEngineTests/UsageTest.html#method_testUsageLimits", "name": "BundleEngineTests\\UsageTest::testUsageLimits", "doc": "&quot;Test if usage is limited successfully.&quot;"},
                    {"type": "Method", "fromName": "BundleEngineTests\\UsageTest", "fromLink": "BundleEngineTests/UsageTest.html", "link": "BundleEngineTests/UsageTest.html#method_testOverdrafts", "name": "BundleEngineTests\\UsageTest::testOverdrafts", "doc": "&quot;Test if usage is correctly overdrafted.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Analytics", "fromLink": "BundleEngine/Model/Analytics.html", "link": "BundleEngine/Model/Analytics/CostContract.html", "name": "BundleEngine\\Model\\Analytics\\CostContract", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Analytics\\CostContract", "fromLink": "BundleEngine/Model/Analytics/CostContract.html", "link": "BundleEngine/Model/Analytics/CostContract.html#method_calculateCostOfGoodsSold", "name": "BundleEngine\\Model\\Analytics\\CostContract::calculateCostOfGoodsSold", "doc": "&quot;Calculate the cost of producing, creating or delivering a product.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html", "name": "BundleEngine\\Model\\Bundle\\Bundle", "doc": "&quot;Main bundle product with all the basic functionality required by a bundle product.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_getMobileData", "name": "BundleEngine\\Model\\Bundle\\Bundle::getMobileData", "doc": "&quot;Get the value of mobileData&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_setMobileData", "name": "BundleEngine\\Model\\Bundle\\Bundle::setMobileData", "doc": "&quot;Set mobile data usage.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_generateInvoice", "name": "BundleEngine\\Model\\Bundle\\Bundle::generateInvoice", "doc": "&quot;Get an invoice for all data used, monthly cost and best bundle insurance.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_calculateCostOfGoodsSold", "name": "BundleEngine\\Model\\Bundle\\Bundle::calculateCostOfGoodsSold", "doc": "&quot;Undocumented function&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_setBestbundleinsurance", "name": "BundleEngine\\Model\\Bundle\\Bundle::setBestbundleinsurance", "doc": "&quot;Set the best bundle insurance on or off.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_getBestBundleInsurance", "name": "BundleEngine\\Model\\Bundle\\Bundle::getBestBundleInsurance", "doc": "&quot;Check if best bundle insurance is active&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method_getName", "name": "BundleEngine\\Model\\Bundle\\Bundle::getName", "doc": "&quot;Get the bundle external name.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Bundle", "fromLink": "BundleEngine/Model/Bundle/Bundle.html", "link": "BundleEngine/Model/Bundle/Bundle.html#method___construct", "name": "BundleEngine\\Model\\Bundle\\Bundle::__construct", "doc": "&quot;Create a new bundle instance&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/BundleA.html", "name": "BundleEngine\\Model\\Bundle\\BundleA", "doc": "&quot;First bundle, no data but low monthly cost.&quot;"},
                    
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/BundleB.html", "name": "BundleEngine\\Model\\Bundle\\BundleB", "doc": "&quot;BundleB contains 1 GB of data for 10 EUR a month&quot;"},
                    
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/BundleC.html", "name": "BundleEngine\\Model\\Bundle\\BundleC", "doc": "&quot;BundleC contains 3 GB of data for 20 EUR a month&quot;"},
                    
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/BundleD.html", "name": "BundleEngine\\Model\\Bundle\\BundleD", "doc": "&quot;BundleD contains 5 GB of data for 30 EUR a month&quot;"},
                    
            {"type": "Class", "fromName": "BundleEngine\\Model\\Bundle", "fromLink": "BundleEngine/Model/Bundle.html", "link": "BundleEngine/Model/Bundle/Usage.html", "name": "BundleEngine\\Model\\Bundle\\Usage", "doc": "&quot;This class tracks usage and allowed usage&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_use", "name": "BundleEngine\\Model\\Bundle\\Usage::use", "doc": "&quot;Adjust usage&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_getOverdraft", "name": "BundleEngine\\Model\\Bundle\\Usage::getOverdraft", "doc": "&quot;Get the amount overdrafted from the allotted amount.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_limitUsageBy", "name": "BundleEngine\\Model\\Bundle\\Usage::limitUsageBy", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_removeUsageLimit", "name": "BundleEngine\\Model\\Bundle\\Usage::removeUsageLimit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_getLimitUsage", "name": "BundleEngine\\Model\\Bundle\\Usage::getLimitUsage", "doc": "&quot;Get limit usage amount&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_setLimitUsage", "name": "BundleEngine\\Model\\Bundle\\Usage::setLimitUsage", "doc": "&quot;Set limit usage amount&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_getUsage", "name": "BundleEngine\\Model\\Bundle\\Usage::getUsage", "doc": "&quot;Get quota used&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_setUsage", "name": "BundleEngine\\Model\\Bundle\\Usage::setUsage", "doc": "&quot;Set quota used&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_getQuota", "name": "BundleEngine\\Model\\Bundle\\Usage::getQuota", "doc": "&quot;Get quota&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method_setQuota", "name": "BundleEngine\\Model\\Bundle\\Usage::setQuota", "doc": "&quot;Set quota&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Bundle\\Usage", "fromLink": "BundleEngine/Model/Bundle/Usage.html", "link": "BundleEngine/Model/Bundle/Usage.html#method___construct", "name": "BundleEngine\\Model\\Bundle\\Usage::__construct", "doc": "&quot;Construct a new usage meter&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model", "fromLink": "BundleEngine/Model.html", "link": "BundleEngine/Model/Customer.html", "name": "BundleEngine\\Model\\Customer", "doc": "&quot;Customer&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_useData", "name": "BundleEngine\\Model\\Customer::useData", "doc": "&quot;Use mobile data.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_setLocation", "name": "BundleEngine\\Model\\Customer::setLocation", "doc": "&quot;Set two letter country code according to ISO 3166-1&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_travelsAbroad", "name": "BundleEngine\\Model\\Customer::travelsAbroad", "doc": "&quot;Check if a user travels abroad.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_returnsFromAbroad", "name": "BundleEngine\\Model\\Customer::returnsFromAbroad", "doc": "&quot;Check if returns from his travels.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_hasBundle", "name": "BundleEngine\\Model\\Customer::hasBundle", "doc": "&quot;Check if a user has an bundle&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_getBundle", "name": "BundleEngine\\Model\\Customer::getBundle", "doc": "&quot;Get bundle&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_setBundle", "name": "BundleEngine\\Model\\Customer::setBundle", "doc": "&quot;Set bundle&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Customer", "fromLink": "BundleEngine/Model/Customer.html", "link": "BundleEngine/Model/Customer.html#method_getLocation", "name": "BundleEngine\\Model\\Customer::getLocation", "doc": "&quot;Get two letter countr code according to ISO 3166-1&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Product", "fromLink": "BundleEngine/Model/Product.html", "link": "BundleEngine/Model/Product/BillableContract.html", "name": "BundleEngine\\Model\\Product\\BillableContract", "doc": "&quot;Make a product billable.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\BillableContract", "fromLink": "BundleEngine/Model/Product/BillableContract.html", "link": "BundleEngine/Model/Product/BillableContract.html#method_generateInvoice", "name": "BundleEngine\\Model\\Product\\BillableContract::generateInvoice", "doc": "&quot;Generate an invoice for this specific bundle&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Product", "fromLink": "BundleEngine/Model/Product.html", "link": "BundleEngine/Model/Product/Invoice.html", "name": "BundleEngine\\Model\\Product\\Invoice", "doc": "&quot;This class exposes an invoice with all invoice lines, descriptions and prices.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\Invoice", "fromLink": "BundleEngine/Model/Product/Invoice.html", "link": "BundleEngine/Model/Product/Invoice.html#method_getInvoiceLines", "name": "BundleEngine\\Model\\Product\\Invoice::getInvoiceLines", "doc": "&quot;Get invoice lines.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\Invoice", "fromLink": "BundleEngine/Model/Product/Invoice.html", "link": "BundleEngine/Model/Product/Invoice.html#method_getInvoiceLineByType", "name": "BundleEngine\\Model\\Product\\Invoice::getInvoiceLineByType", "doc": "&quot;Get the first invoice line of a certain typ&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\Invoice", "fromLink": "BundleEngine/Model/Product/Invoice.html", "link": "BundleEngine/Model/Product/Invoice.html#method_addLine", "name": "BundleEngine\\Model\\Product\\Invoice::addLine", "doc": "&quot;Add a new line to the invoice.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\Invoice", "fromLink": "BundleEngine/Model/Product/Invoice.html", "link": "BundleEngine/Model/Product/Invoice.html#method_calculateNett", "name": "BundleEngine\\Model\\Product\\Invoice::calculateNett", "doc": "&quot;Calculate the amount due.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\Invoice", "fromLink": "BundleEngine/Model/Product/Invoice.html", "link": "BundleEngine/Model/Product/Invoice.html#method___toString", "name": "BundleEngine\\Model\\Product\\Invoice::__toString", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Model\\Product", "fromLink": "BundleEngine/Model/Product.html", "link": "BundleEngine/Model/Product/InvoiceLine.html", "name": "BundleEngine\\Model\\Product\\InvoiceLine", "doc": "&quot;A invoice line justifying the price you pay for a specific product or service.&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method_getDescription", "name": "BundleEngine\\Model\\Product\\InvoiceLine::getDescription", "doc": "&quot;Return description.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method_getAmount", "name": "BundleEngine\\Model\\Product\\InvoiceLine::getAmount", "doc": "&quot;Return amount.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method_getPrice", "name": "BundleEngine\\Model\\Product\\InvoiceLine::getPrice", "doc": "&quot;Return Price.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method___toString", "name": "BundleEngine\\Model\\Product\\InvoiceLine::__toString", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method_getType", "name": "BundleEngine\\Model\\Product\\InvoiceLine::getType", "doc": "&quot;Get invoice line type.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Model\\Product\\InvoiceLine", "fromLink": "BundleEngine/Model/Product/InvoiceLine.html", "link": "BundleEngine/Model/Product/InvoiceLine.html#method___construct", "name": "BundleEngine\\Model\\Product\\InvoiceLine::__construct", "doc": "&quot;Create a new invoice line.&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Repository", "fromLink": "BundleEngine/Repository.html", "link": "BundleEngine/Repository/Customer.html", "name": "BundleEngine\\Repository\\Customer", "doc": "&quot;The customer repository is resposible for bridging the input and output of the\ndata access layer and the local model. It has not been implemented in this example&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_get", "name": "BundleEngine\\Repository\\Customer::get", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_find", "name": "BundleEngine\\Repository\\Customer::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_list", "name": "BundleEngine\\Repository\\Customer::list", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_listWhere", "name": "BundleEngine\\Repository\\Customer::listWhere", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_save", "name": "BundleEngine\\Repository\\Customer::save", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Repository\\Customer", "fromLink": "BundleEngine/Repository/Customer.html", "link": "BundleEngine/Repository/Customer.html#method_delete", "name": "BundleEngine\\Repository\\Customer::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Service", "fromLink": "BundleEngine/Service.html", "link": "BundleEngine/Service/BestBundle.html", "name": "BundleEngine\\Service\\BestBundle", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Service\\BestBundle", "fromLink": "BundleEngine/Service/BestBundle.html", "link": "BundleEngine/Service/BestBundle.html#method_selectBestBundle", "name": "BundleEngine\\Service\\BestBundle::selectBestBundle", "doc": "&quot;Select the best bundle.&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Service\\BestBundle", "fromLink": "BundleEngine/Service/BestBundle.html", "link": "BundleEngine/Service/BestBundle.html#method_isBundleBetter", "name": "BundleEngine\\Service\\BestBundle::isBundleBetter", "doc": "&quot;Check if the new bundle is better than the old&quot;"},
                    {"type": "Method", "fromName": "BundleEngine\\Service\\BestBundle", "fromLink": "BundleEngine/Service/BestBundle.html", "link": "BundleEngine/Service/BestBundle.html#method_generateMockBundles", "name": "BundleEngine\\Service\\BestBundle::generateMockBundles", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "BundleEngine\\Service", "fromLink": "BundleEngine/Service.html", "link": "BundleEngine/Service/BundleProfitRanker.html", "name": "BundleEngine\\Service\\BundleProfitRanker", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "BundleEngine\\Service\\BundleProfitRanker", "fromLink": "BundleEngine/Service/BundleProfitRanker.html", "link": "BundleEngine/Service/BundleProfitRanker.html#method_rankBundleProfit", "name": "BundleEngine\\Service\\BundleProfitRanker::rankBundleProfit", "doc": "&quot;Rank bundles according to nett profit based on data usage&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


