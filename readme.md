# Asignment

## Requirements
This component requires PHP >7.0, Composer. For tooling and generating documentation, Sami needs to be installed and globally available

## Installation
To install this software, normally you would've followed the following steps

Pull the repository

```
git clone <url> ./
```


Pull in the dependencies

```
yarn install
composer install
```

To run the test cases
```
yarn run:test
```

To generate the documentation
```
yarn run:doc
```
## Documentation
Full API documentation is available in the directory `./Docs/Build/index.html`
## Usage - invoice calculator
The intention of this package is to be integrated into a larger system, but a simple api consumer has been created to calculate an invoice per bundle.

You can access this by running `php index.php` and the syntax will by echoed. The syntax is as follows:
```
php index.php <Bundle> <Best Bundle> <Data Usage in MB>
```

an example would be:
```
php index.php BundleA 1 1000
```

## Usage - profitabbility calculator
A very simple consumer of the profitability calculator service has been made. To regenerate the profitability matrix in excel, run
```
php profitCalc.php
```

Find the final profitability matrix in profit.csv