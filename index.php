<?php
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Bundle\Bundle;

require 'bootstrap.php';

$allowedBundles = ['BundleA', 'BundleB', 'BundleC', 'BundleD'];

if ($argc !=4 || !in_array($argv['1'], $allowedBundles))
	die("\r\nUsage:
php index.php <BundleA|BundleB|BundleC|BundleD> <BestBundle:0|1> <Data Usage in MB>\r\n
example:\r\nphp index.php BundleA 1 5000
		 ");

$customer = new Customer();

$bundle = $argv[1];
$bundle = "BundleEngine\\Model\\Bundle\\{$bundle}";
$bundle = new $bundle;

$customer->setBundle($bundle);
$customer->getBundle()->setBestbundleinsurance($argv[2]);
$customer->useData($argv[3] * Usage::DATA_MB);

$invoice = $customer->getBundle()->generateInvoice();
echo $invoice;

echo 'Nett: ', $invoice->calculateNett() / 100 , ' EUR';
echo "\r\n\r\nCost of goods sold: ", $customer->getBundle()->calculateCostOfGoodsSold()  / 100 , ' EUR';
echo "\r\nProfit: ", ($invoice->calculateNett() - $customer->getBundle()->calculateCostOfGoodsSold())  / 100 , ' EUR';
