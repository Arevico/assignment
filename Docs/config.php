<?php
$iterator = Symfony\Component\Finder\Finder::create()
	->files()
	->name('*.php')
	->in('./App/')
	->in('./Tests/');
	// ->in();

$options = [
'theme'                => 'default',
'title'                => 'Laravel API Documentation',
'build_dir'            => __DIR__ . '/build/',
'cache_dir'            => __DIR__ . '/cache/',
];

$sami = new Sami\Sami($iterator, $options);

return $sami;
