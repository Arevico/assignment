<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Bundle\Bundle;
use BundleEngine\Model\Bundle\BundleA;
use BundleEngine\Model\Bundle\BundleB;
use BundleEngine\Model\Bundle\BundleC;
use BundleEngine\Model\Bundle\BundleD;

/**
 * Integration tests from the customer perspective.
 */
final class BundleTest extends TestCase
{

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function testBundle(){
		$bundleA = new BundleA();
		$bundleB = new BundleB();
		$bundleC = new BundleC();
		$bundleD = new BundleD();

		$this->runBundleTest($bundleA);
		$this->runBundleTest($bundleB);
		$this->runBundleTest($bundleC);
		$this->runBundleTest($bundleD);
	}

	/**
	 * Run test cases on each bundle
	 *
	 * @param Bundle $bundle
	 * @return void
	 */
	private function runBundleTest($bundle){
		$this->assertInstanceOf('\\BundleEngine\\Model\\Bundle\\Bundle', $bundle);
		$this->assertInstanceOf('\\BundleEngine\\Model\\Bundle\\Usage', $bundle->getMobileData() );
	}
	
}