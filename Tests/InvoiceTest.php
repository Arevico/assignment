<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Product\Invoice;
use BundleEngine\Model\Product\InvoiceLine;


/**
 * Test the invoicing system.
 */
final class InvoiceTest extends TestCase
{

	/**
	 * Test if invoicing works properly.
	 *
	 * @return void
	 */
	public function testInvoice(){
		$invoice = new Invoice();

		$this->assertCount(0, $invoice->getInvoiceLines());
		$this->assertEquals(0, $invoice->calculateNett() );

		$invoice->addLine(
			new InvoiceLine(InvoiceLine::TYPE_MONTHLY_COST, '-', '-', 100)
		);

		$this->assertEquals(100, $invoice->calculateNett() );

		$invoice->addLine(
			new InvoiceLine(InvoiceLine::TYPE_MONTHLY_COST, '-', '-', 100)
		);

		$this->assertEquals(200, $invoice->calculateNett() );

		$invoice->addLine(
			new InvoiceLine(InvoiceLine::TYPE_MONTHLY_COST, '-', '-', -100)
		);

		$this->assertEquals(100, $invoice->calculateNett() );
		$this->assertCount(3, $invoice->getInvoiceLines());


	}

}