<?php
namespace BundleEngine\Model\Bundle;

/**
 * BundleC contains 3 GB of data for 20 EUR a month
 */
class BundleC extends Bundle {

	protected $dataQuota 		= 3 * 1000 * 1000;
	protected $monthlyCost 	 	= 2000;

}