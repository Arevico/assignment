<?php
namespace BundleEngine\Model\Product;

use BundleEngine\Model\Bundle\Bundle;
use BundleEngine\Model\Product\InvoiceLine;
use BundleEngine\Model\Bundle\Usage\Usage;

/**
 * This class exposes an invoice with all invoice lines, descriptions and prices.
 * 
 */
class Invoice {

	/**
	 * Undocumented variable
	 *
	 * @var InvoiceLine[]
	*/
	protected $invoiceLines = [];

	/**
	 * Get invoice lines.
	 *
	 * @return InvoiceLine[]
	 */
	public function getInvoiceLines(){
		return $this->invoiceLines;
	}

	/**
	 * Get the first invoice line of a certain typ
	 *
	 * @param string $type The InvoiceLine type.
	 * @return InvoiceLine
	 */
	public function getInvoiceLineByType($type){
		foreach ($this->getInvoiceLines() as $line)
		{
			if ($line->getType() == $type)
				return $line;
		}
			
		return null;
	}

	/**
	 * Add a new line to the invoice.
	 *
	 * @param InvoiceLine $line
	 * @param string (optional) $type associate with an invoice line type
	 */
	public function addLine(InvoiceLine $line){
		$this->invoiceLines[] = $line;
	}
	/**
	 * Calculate the amount due.
	 *
	 * @return integer
	 */
	public function calculateNett(){
		$amount = 0;

		foreach ($this->getInvoiceLines() as $line)
			$amount += $line->getPrice();

		return $amount;
	}

	public function __toString()
	{
		$invoice = '';

		foreach ($this->getInvoiceLines() as $line)
			$invoice .= "{$line}\n";

		return $invoice;
	}
}