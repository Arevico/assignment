<?php
namespace  BundleEngine\Service;

use BundleEngine\Model\Bundle;

class BestBundle{

	private $bundles = [];

	/**
	 * Select the best bundle.
	 *
	 * @param Bundle\Bundle $current The current bundle
	 * @return boolean|Bundle\Bundle Returns a bundle object if an better bundle was found, otherwise true
	 */
	public function selectBestBundle(Bundle\Bundle $current){
		$bestBundle = clone $current; 	//Shallow clone, modify primitives only or implement __
		$bestBundle->setBestbundleinsurance(false);

		$dataUsage = $current->getMobileData()->getUsage();

		$this->generateMockBundles();

		foreach ($this->bundles as $bundle) {
			if ($this->isBundleBetter($bestBundle, $bundle))
				$bestBundle = $bundle;

		}
		
		return get_class($bestBundle) == get_class($current) ? true : $bestBundle;
	}

	/**
	 * Check if the new bundle is better than the old
	 *
	 * @param Bundle\Bundle $current Current bundle. Make sure that setBestbundleinsurance is off.
	 * @param Bundle\Bundle $new Bundle to test against. Make sure that setBestbundleinsurance is off.
	 * @return boolean
	 */
	public function isBundleBetter(Bundle\Bundle $current, Bundle\Bundle $new){
		$new->getMobileData()->setUsage( $current->getMobileData()->getUsage());

		$currentPrice = $current->generateInvoice()->calculateNett();
		$newPrice 	  = $new->generateInvoice()->calculateNett();

		return $newPrice < $currentPrice;
	}

	public function generateMockBundles(){
		$this->bundles[] = new Bundle\BundleA();
		$this->bundles[] = new Bundle\BundleB();
		$this->bundles[] = new Bundle\BundleC();
		$this->bundles[] = new Bundle\BundleD();
	}

}