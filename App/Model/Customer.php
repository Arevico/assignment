<?php
namespace BundleEngine\Model;

use BundleEngine\Model\Bundle\Bundle;
use BundleEngine\Model\Bundle\Usage;

/**
 * Customer
 */
class Customer{

	/**
	 * Bundle
	 *
	 * @var Bundle
	 */
	protected $bundle;

	/**
	 * Sim number
	 * 
	 * @var integer 
	 */
	protected $sim;

	/**
	 * Two letter countr code according to ISO 3166-1
	 *
	 * @var string
	 */
	protected $location = 'NL';

	/**
	 * Use mobile data.
	 *
	 * @param integer $amount Data in KB
	 * @return bool
	 */
	public function useData($amount){
		return $this->hasBundle() && $this->getBundle()->getMobileData()->use($amount);
	}

	/**
	 * Set two letter country code according to ISO 3166-1
	 *
	 * @param string $newLocation  Two letter country code according to ISO 3166-1
	 */ 
	public function setLocation($newLocation)
	{
		if ($this->hasBundle() && $this->travelsAbroad($newLocation) ){ // Legally required to limit data usage
			$this->getBundle()->getMobileData()->limitUsageBy(50 * Usage::DATA_MB);
		
		} elseif ($this->hasBundle() && $this->returnsFromAbroad($newLocation)) {
			$this->getBundle()->getMobileData()->removeUsageLimit();
		}

		$this->location = $newLocation;
	}

	/**
	 * Check if a user travels abroad.
	 *
	 * @param string $country Two letter country code.
	 * @return void
	 */
	public function travelsAbroad($newLocation){
		return ($this->location == 'NL' && $newLocation != 'NL');
	}

	/**
	 * Check if returns from his travels.
	 *
	 * @param string $country Two letter country code.
	 * @return void
	 */
	public function returnsFromAbroad($newLocation){
		return ($this->location != 'NL' && $newLocation == 'NL');
	}
	/**
	 * Check if a user has an bundle
	 *
	 * @return boolean
	 */
	public function hasBundle(){
		return $this->getBundle() != null;
	}

	/**
	 * Get bundle
	 *
	 * @return Bundle
	 */ 
	public function getBundle()
	{
		return $this->bundle;
	}

	/**
	 * Set bundle
	 *
	 * @param  Bundle  $bundle  Bundle
	 *
	 */ 
	public function setBundle(Bundle $bundle)
	{
		$this->bundle = $bundle;
	}

	/**
	 * Get two letter countr code according to ISO 3166-1
	 *
	 * @return  string
	 */ 
	public function getLocation()
	{
		return $this->location;
	}


}
