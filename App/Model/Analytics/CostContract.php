<?php
namespace BundleEngine\Model\Analytics;


interface CostContract {

	/**
	 * Calculate the cost of producing, creating or delivering a product.
	 *
	 * @return void
	 */
	function calculateCostOfGoodsSold();
}