<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Bundle\BundleA;
use BundleEngine\Model\Bundle\BundleD;
use BundleEngine\Service\BestBundle;

/**
 * Integration tests from the customer perspective.
 */
final class BestBundleTest extends TestCase
{

	/**
	 * Test if a better bundle can be found and if the best bundle can be selected.
	 *
	 * @return void
	 */
	public function testBetterBundle(){
		$bestBundleService = new BestBundle();
		
		$worstBundle = new BundleA();
		$worstBundle->getMobileData()->use(5 * Usage::DATA_GB);

		$bestBundle = new BundleD();
		$bestBundle->getMobileData()->use(5 * Usage::DATA_GB);

		$this->assertTrue($bestBundleService->isBundleBetter($worstBundle, $bestBundle));
		$this->assertfalse($bestBundleService->isBundleBetter($bestBundle, $worstBundle));

		$bestBundle =  $bestBundleService->selectBestBundle($worstBundle);
		
		if (!$this->assertNotEquals(true, $bestBundle))
			$this->assertInstanceOf(BundleD::class, $bestBundle);
	}

}