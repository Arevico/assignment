<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Bundle\BundleA;

/**
 * Test the usage object.
 */
final class UsageTest extends TestCase
{
	/**
	 * Test if usage is limited successfully.
	 *
	 * @return void
	 */
	public function testUsageLimits(){
		$usage = new Usage(1 * Usage::DATA_GB);

		$this->assertEquals( 1000000, $usage->getQuota());

		$usage->setLimitUsage( $usage->getQuota() );
		$this->assertEquals(1000000, $usage->getLimitUsage(), 'Limit has not been set.');
	
		$this->assertFalse( $usage->use(2 * Usage::DATA_GB), 'Cann\'t use more data than limited!');

	}

	/**
	 * Test if usage is correctly overdrafted.
	 *
	 * @return void
	 */
	public function testOverdrafts(){
		$usage = new Usage(1 * Usage::DATA_GB);
		$this->assertEquals(0, $usage->getOverdraft(), 'Overdraft detected when it should not be detected!');

		$usage->use(2 * Usage::DATA_GB);
		$this->assertEquals(1000000, $usage->getOverdraft(),'Overdraft is not correctly calculated');
	}

}