<?php
namespace BundleEngine\Model\Product;

use BundleEngine\Model\Bundle\Bundle;
use BundleEngine\Model\Bundle\Usage\Usage;

/**
 * Make a product billable.
 * 
 */
interface BillableContract{

	/**
	 * Generate an invoice for this specific bundle
	 *
	 * @return Invoice
	 */
	public function generateInvoice();	

}