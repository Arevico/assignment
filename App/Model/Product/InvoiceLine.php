<?php
namespace BundleEngine\Model\Product;

use BundleEngine\Model\Bundle\Bundle;
use BundleEngine\Model\Bundle\Usage\Usage;

/**
 * A invoice line justifying the price you pay for a specific product or service.
 * 
 */
class InvoiceLine {

	/**
	 * Type of invoice line.
	 *
	 * @var string
	 */
	protected $type;

	/**
	 * Description for the specific amount.
	 *
	 * @var string
	 */
	protected $description;

	/**
	 * Units consumed
	 *
	 * @var string
	 */
	protected $amount;

	/**
	 * Total Price
	 *
	 * @var integer
	 */
	protected $price;

	const TYPE_BEST_BUNDLE_COST 	= 'best_bundle_cost';
	const TYPE_BEST_BUNDLE_DISCOUNT = 'best_bundle_rebate';
	const TYPE_MONTHLY_COST 	= 'monthly_cost';
	const TYPE_OVERDRAFT_COST 	= 'over_draft_cost';

	/**
	 * Return description.
	 *
	 * @return string
	 */
	public function getDescription(){
		return $this->description;
	}

	/**
	 * Return amount.
	 *
	 * @return string
	 */
	public function getAmount(){
		return $this->amount;
	}

	/**
	 * Return Price.
	 *
	 * @return integer
	 */
	public function getPrice(){
		return $this->price;
	}

	public function __toString(){
		$price = $this->price / 100;
		return "{$this->getDescription()}, {$this->getAmount()}, $price EUR";
	}

	/**
	 * Get invoice line type.
	 *
	 * @return string
	 */
	public function getType(){
		return $this->type;
	}
	
	/**
	 * Create a new invoice line.
	 *
	 * @param string $description description
	 * @param string $amount units consumed
	 * @param integer $price total price
	 */
	public function __construct($type, $description, $amount, $price )
	{
		$this->type			= $type;
		$this->description	= $description;
		$this->amount 		= $amount;
		$this->price 		= $price;
	}
}

	