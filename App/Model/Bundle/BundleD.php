<?php
namespace BundleEngine\Model\Bundle;

/**
 * BundleD contains 5 GB of data for 30 EUR a month
 */
class BundleD extends Bundle {

	protected $dataQuota 		= 5 * 1000 * 1000;
	protected $monthlyCost 	 	= 3000;

}