<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Bundle\BundleA;

/**
 * Integration tests from the customer perspective.
 */
final class CustomerTest extends TestCase
{

	/**
	 * 	Test data usage when a customer travels abroad.
	 *
	 * @return void
	 */
	public function testDataUsageAbroad(){
		$customer = new Customer();
		$customer->setBundle(new BundleA());

		$this->assertEquals('NL', $customer->getLocation());

		$customer->setLocation('CH');
		$this->assertFalse($customer->useData(51 * Usage::DATA_MB), 'No more than 50 MB may be used abroad');
		
		$this->assertTrue($customer->useData(49 * Usage::DATA_MB), '50 MB may be used abroad');
		$this->assertFalse($customer->useData(2 * Usage::DATA_MB), 'The next 2MB may not be used!');

		$customer->setLocation('NL'); // Back home
		
		$this->assertTrue($customer->useData(5 * Usage::DATA_GB , 'The next 2MB may be used!') );

	}

}