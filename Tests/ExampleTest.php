<?php
namespace BundleEngineTests;

use PHPUnit\Framework\TestCase;
use BundleEngine\Model\Customer;
use BundleEngine\Model\Bundle;
use BundleEngine\Model\Bundle\Usage;
use BundleEngine\Model\Product\InvoiceLine;

/**
 * This test cases are to verify the example to check if the implementation
 * is valid for the example given in the assessment pdf.
 */
final class ExampleTest extends TestCase
{

	/**
	 * Test bundleD case named in the pdf.
	 *
	 * @return void
	 */
    public function testExampleBundleC()
    {
		$customer 	= new Customer();

		$Bundle= new Bundle\BundleC;
		
		$customer->setBundle($Bundle);
		$customer->getBundle()->setBestbundleinsurance(true);
		
		$customer->useData( 0.9 * Usage::DATA_GB) ;

		$invoice = $customer->getBundle()->generateInvoice();

		$rebate 	= $invoice->getInvoiceLineByType(InvoiceLine::TYPE_BEST_BUNDLE_DISCOUNT);

		$this->assertNotNull($rebate, 'No best bundle rebate found');

		$this->assertEquals(-1000, $rebate->getPrice(), 'Wrong rebate amount!');
		$this->assertEquals('BundleB', $rebate->getAmount(), 'Wrong best bundle selected!');
		$this->assertEquals(1500, $invoice->calculateNett(), 'Wrong total amount!');

	}

	/**
	 * Test bundleD case named in the pdf.
	 *
	 * @return void
	 */
	public function testExampleBundleD()
    {
		$customer 	= new Customer();

		$Bundle= new Bundle\BundleD;
		
		$customer->setBundle($Bundle);
		$customer->getBundle()->setBestbundleinsurance(true);
		
		$customer->useData( 1 * Usage::DATA_GB) ;
		$customer->useData( 3 * Usage::DATA_MB) ;

		$invoice = $customer->getBundle()->generateInvoice();

		$rebate 	= $invoice->getInvoiceLineByType(InvoiceLine::TYPE_BEST_BUNDLE_DISCOUNT);

		$this->assertNotNull($rebate, 'No best bundle rebate found');

		$this->assertEquals(-1970, $rebate->getPrice(), 'Wrong rebate amount!');
		$this->assertEquals('BundleB', $rebate->getAmount(), 'Wrong best bundle selected!');
		$this->assertEquals(1530, $invoice->calculateNett(), 'Wrong total amount!');
	}

}